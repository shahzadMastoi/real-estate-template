// import { Col, Row } from 'antd';
// import Link from 'next/link';
// import style from '@/styles/Home.module.scss';

// const Navbar = () => {
//   return (
//     <div className={style.navbar}>
//       <Row justify="center" style={{gap:'50px'}}>
//         <Col xs={1} sm={1} md={2} lg={2} className={style.linkcol}>
//           <Link href="/">Home</Link>
//         </Col>
//         <Col xs={1} sm={1} md={1} lg={2} className={style.linkcol}>
//           <Link href="/Landing">Landing</Link>
//         </Col>
//         <Col xs={1} sm={1} md={2} lg={2} className={style.linkcol}>
//           <Link href="/About">About</Link>
//         </Col>
//         <Col xs={1} sm={1} md={2} lg={2} className={style.linkcol}>
//           <Link href="/Team">Team</Link>
//         </Col>
//         <Col xs={1} sm={1} md={2} lg={2} className={style.linkcol}>
//           <Link href="">Gallery</Link>
//         </Col>
//         <Col xs={1} sm={1} md={2} lg={2} className={style.linkcol}>
//           <Link href="/Contact">Contact</Link>
//         </Col>
//       </Row>
//     </div>
//   );
// };

// export default Navbar;


import { Menu, Drawer, Button, Col, Row } from 'antd';
import { MenuOutlined } from '@ant-design/icons';
import Link from 'next/link';
import style from '@/styles/Home.module.scss';
import React from 'react';

const Navbar = () => {
  const [visible, setVisible] = React.useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className={style.navbar}>
      <div className={style.mobileNav}>
        <Button
          type="text"
          icon={<MenuOutlined />}
          onClick={showDrawer}
        />
        <Drawer
          title="Navigation"
          placement="left"
          style={{ width: '200px' }}
          closable={false}
          onClose={onClose}
          visible={visible}
        >
          <div className={style.drawerMenu}>
            <Menu mode="vertical">
              <Menu.Item key="1">
                <Link href="/">Home</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link href="/Landing">Landing</Link>
              </Menu.Item>
              <Menu.Item key="3">
                <Link href="/About">About</Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link href="/Team">Team</Link>
              </Menu.Item>
              <Menu.Item key="5">
                <Link href="">Gallery</Link>
              </Menu.Item>
              <Menu.Item key="6">
                <Link href="/Contact">Contact</Link>
              </Menu.Item>
            </Menu>
          </div>
        </Drawer>
      </div>
      <div className={style.desktopScreen}>
        <Row justify="center" style={{ gap: '50px' }}>
          <Col xs={1} sm={1} md={1} lg={2} className={style.linkcol}>
            <Link href="/">Home</Link>
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} className={style.linkcol}>
            <Link href="/Landing">Landing</Link>
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} className={style.linkcol}>
            <Link href="/About">About</Link>
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} className={style.linkcol}>
            <Link href="/Team">Team</Link>
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} className={style.linkcol}>
            <Link href="">Gallery</Link>
          </Col>
          <Col xs={1} sm={1} md={1} lg={2} className={style.linkcol}>
            <Link href="/Contact">Contact</Link>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Navbar;

