import Link from 'next/link'
import { Col, Input, Button, Card, Space, Row, Carousel } from 'antd'
import 'bootstrap/dist/css/bootstrap.css'
import { Typography } from 'antd';
import { FcIdea, FcNext, FcPrevious } from 'react-icons/fc';
import { AiFillSetting } from 'react-icons/ai';
import { FaUserFriends } from 'react-icons/fa';
import style from './index.module.scss';
import Navbar from '@/pages/Components/navbar';
import CustomCard from '../../Common Components/CustomCard';
import NumberOf from '../../Common Components/NumberOf/numberOf';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import { useEffect, useState } from 'react';

const { Meta } = Card;
const { Title } = Typography;
export default function Home() {
  const [startIndex, setStartIndex] = useState(0); // State to track the starting index of displayed cards
  const [animation, setAnimation] = useState(false); // State to track the starting index of displayed cards
  // const [scrollPosition, setScrollPosition] = useState(0);
  // const [viewportHeight, setViewportHeight] = useState(0);
  // const [startCounting, setStartCounting] = useState(false);
  // const [count, setCount] = useState(0);

  // useEffect(() => {
  //   const handleScroll = () => {
  //     setScrollPosition(window.scrollY || window.pageYOffset);
  //     setViewportHeight(window.innerHeight || document.documentElement.clientHeight);

  //     const triggerPoint = 1897; // Adjust this to the scroll position where you want to start counting
  //     if (scrollPosition >= triggerPoint && !startCounting) {
  //       setStartCounting(true);
  //     }
  //   };

  //   window.addEventListener('scroll', handleScroll);
  //   handleScroll(); // Initial call to set initial values

  //   return () => {
  //     window.removeEventListener('scroll', handleScroll);
  //   };
  // }, [scrollPosition, startCounting]);

  // const targetValue = 300;
  // useEffect(() => {
  //   if (startCounting && count < targetValue) {
  //     const interval = setInterval(() => {
  //       setCount(prevCount => prevCount + 1);
  //     }, 50); // Adjust the interval duration as needed

  //     return () => clearInterval(interval);
  //   }
  // }, [startCounting, count]);




  const handleNextClick = () => {
    setAnimation(false)
    setStartIndex((prevIndex) => Math.min(prevIndex + 3, Cards.length - 3));
    console.log(startIndex,'next');
    
  };
  const handlePreviousClick = () => {
    setStartIndex((prevIndex) => prevIndex - 3);
    console.log(startIndex,'prev');

  };

  const Cards = [
    {
      title: "Card title 1",
      description: "This is the description 1",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 2",
      description: "This is the description 2",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 3",
      description: "This is the description 3",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 4",
      description: "This is the description 3",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 5",
      description: "This is the description 3",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 6",
      description: "This is the description 3",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 6",
      description: "This is the description 3",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 6",
      description: "This is the description 3",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },
    {
      title: "Card title 6",
      description: "This is the description 3",
      imageUrl: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    }
  ];

  const displayedCards = Cards.slice(startIndex, startIndex + 3);
console.log(displayedCards);

  return (
    <div>
      <Navbar></Navbar>
      <div className={style.maindiv}>
        <div className={style.setdiv}>
          <Title>Easy Way to Find a Perfect Property</Title>
          <p>Sample text. Click to select the Text Element.</p>
          {/* <Row className={style.setitem} gutter={3}> */}
          <Row justify="center" style={{ padding: '30px' }}>
            <Col lg={10} md={18} sm={24} xs={24} style={{ minWidth: '50vw' }}>
              <div style={{ background: 'red', padding: '30px', borderRadius: '8px', display: 'flex', alignItems: 'center' }}>
                <Col lg={12} md={12} sm={24} xs={24} style={{ paddingRight: '10px' }}>
                  <Input size='large' style={{ borderRadius: '2em' }} placeholder='Enter a valid email address' />
                </Col>
                <Col lg={12} md={12} sm={24} xs={24} style={{ paddingLeft: '10px' }}>
                  <Button shape='round' size='large'>
                    Submit
                  </Button>
                </Col>
              </div>
            </Col>
          </Row>
          {/* </Row> */}
        </div>
        <div style={{ padding: '0 20px' }}>
          <div className='d-flex flex-wrap gap-4 mt-4' style={{ overflow: 'auto', position: 'relative' }}>
            {displayedCards.map((card, index) => (
              <div key={startIndex + index} style={{ flex: '0 0 calc(33.33% - 20px)',transition: animation === true ? 'opacity .5s ease-in-out':'',opacity: animation ? 1:'' }}>
                <Card cover={<img alt="example" src={card.imageUrl} />}>
                  <Meta title={card.title} description={card.description} />
                </Card>
              </div>
            ))}
          </div>
          {startIndex + 3 < Cards.length && ( // Display "Next" button if there are more cards to show
            <div style={{ position: 'absolute', textAlign: 'center', marginTop: '-16vmax', right: '0' }}>
              <FcNext style={{ fontSize: '2rem', cursor: 'pointer' }} onClick={handleNextClick} />
            </div>
          )}

          {startIndex > 0 && ( // Display "Next" button if there are more cards to show
            <div style={{ position: 'absolute', textAlign: 'center', marginTop: '-16vmax',left:'0' }}>
              <FcPrevious style={{ fontSize: '2rem', cursor: 'pointer' }} onClick={handlePreviousClick} />
            </div>
          )}
        </div>


        <div className='mt-4 gap-4'>
          <Row gutter={4}>
            <Col lg={11} md={11} sm={24} xs={24}>
              <Card              >
                <Title>Let’s Find The Right Selling Option</Title>
                <div className={style.icons}>
                  <div className='d-flex gap-4 align-items-center'>
                    <FcIdea style={{ width: '100px' }} />
                    <div>
                      <h3>Property Management</h3>
                      <p>Sample text. Click to select the Text Element.</p>
                    </div>
                  </div>
                  <div className='d-flex gap-4 align-items-center'>
                    <AiFillSetting style={{ width: '100px' }} />
                    <div>
                      <h3>Property Management</h3>
                      <p>Sample text. Click to select the Text Element.</p>
                    </div>
                  </div>
                  <div className='d-flex gap-4 align-items-center'>
                    <FaUserFriends style={{ width: '100px' }} />
                    <div>
                      <h3>Property Management</h3>
                      <p>Sample text. Click to select the Text Element.</p>
                    </div>
                  </div>
                </div>
              </Card>
            </Col>
            <Col span={1}></Col>
            <Col lg={11} md={11} sm={24} xs={24} style={{ maxHeight: 'auto' }}>
              <Card
                hoverable
                cover={<img alt="example" className={style.cardImage} src="https://assets.nicepagecdn.com/11a8ddce/4460241/images/hand-holding-house-real-estate-property-model.jpg" />}
              >
              </Card>
            </Col>
          </Row>
        </div>
      </div>
      {/* { */}
        {/* // a && ( */}

      <div className='d-flex justify-content-around align-item-center' style={{ background: '#fbeced', padding: '50px', flexWrap: 'wrap' }}>
        <NumberOf value={1200} text='my name is arbaz'></NumberOf>
        <NumberOf value={1200} text='my name is arbaz'></NumberOf>
        <NumberOf value={1200} text='my name is arbaz'></NumberOf>
      </div>
        {/* )
      } */}
      <br />
      <div className='mt-4'>
        <h1 style={{ textAlign: 'center' }}>Explore Apartment Types</h1>
        <div className='mt-4'>
          <Row gutter={30} className='justify-content-center mt-4'>
            <CustomCard title='Select Item' imageUrl='https://assets.nicepagecdn.com/11a8ddce/4460241/images/hand-holding-house-real-estate-property-model.jpg' />
            <CustomCard title='Select Item' imageUrl='https://assets.nicepagecdn.com/11a8ddce/4460241/images/hand-holding-house-real-estate-property-model.jpg' />
            <CustomCard title='Select Item' imageUrl='https://assets.nicepagecdn.com/11a8ddce/4460241/images/hand-holding-house-real-estate-property-model.jpg' />
          </Row>
        </div>
      </div>
    </div>
  )
}
