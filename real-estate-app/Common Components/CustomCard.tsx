import React, { FC } from 'react';
import { Col, Card } from 'antd'; // Assuming you're using Ant Design

interface CustomCardProps {
  background?: string;
  color?: string;
  title?: string;
  imageUrl?: string;
}

const CustomCard: FC<CustomCardProps> = ({ background, color, title, imageUrl }) => {
  const cardStyle = {
    background: background || '#f0effd',
    border: 'none',
    padding: '50px',
    borderRadius: '8px',
  };

  const imageStyle = {
    borderRadius: '8px',
  };

  return (
    <Col lg={7} md={11} sm={24} xs={24}>
      <div style={cardStyle}>
        <Card style={{ border: 'none', background:'#f0effd' }} 
        cover={imageUrl ? <img alt="example" style={imageStyle} src={imageUrl} /> : null}>
          <Card.Meta title={title || 'Default Title'} style={{ color }} />
        </Card>
      </div>
    </Col>
  );
};

export default CustomCard;
