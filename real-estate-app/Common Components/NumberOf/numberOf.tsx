import { FC } from "react"

interface CustomProps {
value?: number,
text?: string
}

const NumberOf: FC<CustomProps> = ({ value,text }) => {

const numberOf = {
    value: value,
    text: text
}

    return(
        <div>
        <div style={{ padding: '40px' }}>
          <h1>{numberOf.value}</h1>
          <h5>{numberOf.text}</h5>
        </div>
      </div>
    )
}

export default NumberOf